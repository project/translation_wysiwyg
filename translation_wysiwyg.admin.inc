<?php

function translation_wysiwyg_settings(&$form_state) {

  $form['translation_wysiwyg_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled types'),
    '#default_value' => variable_get('translation_wysiwyg_types', array(NULL => NULL)),
    '#options' => array_map('check_plain', node_get_types('names')),
    '#description' => t('Choose which node types which should not show a Wysiwyg editor to translators.'),
  );

  return system_settings_form($form);
}

